<?php
namespace Lib;
class LogFile{
    function setFiles($files){
        $this->files = $files;
        return $this;
    }

    function reformat(){
        $data = [];
        $fileClass = new \Lib\File();
        foreach ($this->files as $name => $files) {
            if(is_array($files)){
                foreach ($files as $file){

                    $contents = $fileClass->setFile($file)->read();
                    $data[$name] = explode("\n", $contents);
                }
            }else{
                $contents = $fileClass->setFile($files)->read();
                $data[$name] = explode("\n", $contents);
            }
        }
        return $data;
    }
}