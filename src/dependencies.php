<?php
// DIC configuration

$container = $app->getContainer();

// view
$container['view'] = function ($c) {
    return new \Slim\Views\PhpRenderer('templates/');
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

// cmd
$container['backend'] = function ($c) {
    $backend = new \Lib\BackendCli();
    return $backend;
};

// file
$container['file'] = function ($c) {
    $file = new \Lib\File();
    return $file;
};

// log
$container['log'] = function ($c) {
    $file = new \Lib\LogFile();
    return $file;
};

// browser
$container['browser'] = function ($c) {
    $browser = new \Lib\Browser();
    return $browser;
};