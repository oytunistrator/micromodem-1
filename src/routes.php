<?php
// Routes
$app->get('/', function ($request, $response, $args) {
    $this->logger->info('New Access: /');
    return $this->view->render($response, 'index.phtml');
});

$app->get('/info', function ($request, $response, $args) {
    $this->logger->info('New Access: /info');

    $data = [];

    $commandArr = [
        'php' => 'php -v',
        'ifconfig' => 'ifconfig',
        'apache' => 'apachectl -v',
        'openssl' => 'openssl version',
        'psql' => 'psql --version',
        'uname' => 'uname -a',
        'lsb_release' => 'lsb_release -a',
        'nginx' => 'nginx -v',
    ];

    $logFiles = [
       'access' => [
           '/var/log/apache2/access_log',
           '/private/var/log/apache2/access_log'
       ],
        'error' => [
            '/var/log/apache2/error_log',
            '/private/var/log/apache2/error_log'
        ],
        'app-log' => [
            'logs/app.log'
        ]
    ];

    $data = $this->backend->cmdArray($commandArr);

    $data['OS'] = PHP_OS;

    $data['userAgent'] = $this->browser->getUserAgent();

    $data['tmpFile'] = tempnam(sys_get_temp_dir(), 'Tux');

    $data['load_avg'] = sys_getloadavg();
    $data['connection_status'] = connection_status();
    $data['extensions'] = get_loaded_extensions();



    $data['log'] = $this->log->setFiles($logFiles)->reformat();

    // Render index view
    return $this->response->withJson($data, 201);;
});