var m = new Metruga();
var nav = m.navigation;

/* Render Sources */
m.render(".main", {
  title: "<a href='/'>Micro Modem</a>",
});

var htmlencode = function(str) {
  return str.replace(/[&<>"']/g, function($0) {
    return "&" + {"&":"amp", "<":"lt", ">":"gt", '"':"quot", "'":"#39"}[$0] + ";";
  });
}

var rootNavHtml = '<div class="list-group">'
    + '<a href="#" class="list-group-item active dashboard">Dashboard</a>'
    + '<a href="#/info" class="list-group-item info">Info</a>'
    + '<a href="#/about" class="list-group-item about">About</a>'
    + '</div>';

m.select("#sidebar").html(rootNavHtml);

var configNavHtml = '<div class="list-group">'
    + '<a href="#/config/system" class="list-group-item active">System</a>'
    + '<a href="#/config/apache" class="list-group-item">Apache</a>'
    + '<a href="#/config/nginx" class="list-group-item">Nginx</a>'
    + '<a href="#/config/mysql" class="list-group-item">Mysql</a>'
    + '<a href="#/config/postgresql" class="list-group-item">Postgresql</a>'
    + '<a href="#/config/pacman" class="list-group-item">Package Manager</a>'
    + '<a href="#/config/modules" class="list-group-item">Module Manager</a>'
    + '</div>';

var infoNavHtml = '<div class="list-group">'
    + '<a href="#/info/system" class="list-group-item active">System</a>'
    + '</div>';

var routeNavHtml = '<div class="list-group">'
    + '<a href="#/route/network" class="list-group-item active">Network Route</a>'
    + '<a href="#/route/firewall" class="list-group-item">Firewall</a>'
    + '</div>';



$(document).ready(function() {
  /* Navbar Config */
  nav.config('#navbar', [
        {
          path: '/config',
          link: {
            text: 'Config'
          },
          fn: function () {
            m.select("#sidebar").html(rootNavHtml);
            m.select('.list-group-item').removeClass('active');
            m.select('.config').addClass('active');
            m.select('.title').html('Config');
            m.select('body').removeClass('loaded');
            var content = "";
            m.select('.content').html(content);
            m.select('body').addClass('loaded');
          }
        },
        {
          path: '/about',
          link: {
            text: 'About'
          },
          fn: function () {
            m.select("#sidebar").html(rootNavHtml);
            m.select('.list-group-item').removeClass('active');
            m.select('.about').addClass('active');
            m.select('.title').html('About');
            m.select('body').removeClass('loaded');
            var content = "";
            m.select('.content').html(content);
            m.select('body').addClass('loaded');
          }
        },
        {
          path: '/route',
          link: {
            text: 'Route'
          },
          fn: function () {
            m.select("#sidebar").html(routeNavHtml);
            m.select('.list-group-item').removeClass('active');
            m.select('.route').addClass('active');
            m.select('.title').html('About');
            m.select('body').removeClass('loaded');
            var content = "";
            m.select('.content').html(content);
            m.select('body').addClass('loaded');
          }
        },
        {
          path: '/info',
          link: {
            text: 'Info'
          },
          fn: function () {
            m.select("#sidebar").html(rootNavHtml);
            m.select('.list-group-item').removeClass('active');
            m.select('.info').addClass('active');
            m.select('.title').html('General Information');
            m.select('body').removeClass('loaded');
            m.xhr({ contentType: 'json' })
            .get('/info')
            .then(function(data){
              var newData = JSON.parse(data);
              var content = "";
              if(newData.apache.length > 0){
                content += '<h3>Apache</h3>'
                for(i in newData.apache) content += '<div>' + htmlencode(newData.apache[i]) + '</div>';
              }
              if(newData.nginx.length > 0) {
                content += '<h3>Nginx</h3>';
                for (i in newData.nginx) content += '<div>' + htmlencode(newData.nginx[i]) + '</div>';
              }
              if(newData.php.length > 0) {
                content += '<h3>PHP</h3>';
                for (i in newData.php) content += '<div>' + htmlencode(newData.php[i]) + '</div>';
                if (newData.extensions.length > 0) {
                  content += '<h5>Extensions</h5>';
                  content += '<ul>';
                  for (i in newData.extensions) content += '<li>' + htmlencode(newData.extensions[i]) + '</li>';
                  content += '</ul>';
                }
              }
              if(newData.psql.length > 0) {
                content += '<h3>Postgresql</h3>'
                for (i in newData.psql) content += '<div>' + htmlencode(newData.psql[i]) + '</div>';
              }
              if(newData.ifconfig.length > 0) {
                content += '<h3>Network</h3>'
                for (i in newData.ifconfig) content += '<div>' + htmlencode(newData.ifconfig[i]) + '</div>';
              }
              m.select('.content').html(content);
              m.select('body').addClass('loaded');
            });
          }
        },
      ], {
        class: 'nav navbar-nav',
        parent: 'ul',
        child: 'li',
        root: {
          fn: function () {
            m.select("#sidebar").html(rootNavHtml);
            m.select('.list-group-item').removeClass('active');
            m.select('.dashboard').addClass('active');
            m.select('.title').html('Dashboard');
            m.select('body').removeClass('loaded');
          }
        }
      }
  );

  setTimeout(function(){
    $('body').addClass('loaded');
    $('h1').css('color','#222222');
  }, 1000);

});